# Documentación del Sensor KY-038 de Proximidad

## Introducción

En esta documentación, proporcionaremos información detallada sobre el sensor KY-038 de proximidad, incluyendo su modelo, descripción y funcionalidades. Esta documentación ha sido elaborada por Pedro Tabisi y Pablo Vera.

### Sensor KY-038

- **Modelo**: KY-038
- **Descripción**: El sensor KY-038 de proximidad es un sensor ultrasónico que utiliza ondas sonoras para medir la distancia entre el sensor y un objeto cercano. Puede ser utilizado en una variedad de aplicaciones, como sistemas de detección de obstáculos, sistemas de seguridad y más.

## Desarrollo

### Conexión del Sensor

Para utilizar el sensor KY-038, debes conectarlo correctamente a tu placa de desarrollo o microcontrolador. A continuación, se muestra un esquema de conexión típico:

![conexion 1](https://blog.uelectronics.com/wp-content/uploads/2021/07/conexion-1-1.jpg)

### Código

A continuación, se presenta un ejemplo de código en [lenguaje de programación] para interactuar con el sensor KY-038 y obtener lecturas de distancia:

```python
# Ejemplo de código en Python para el sensor KY-038 de proximidad
import RPi.GPIO as GPIO
import time

TRIG = 23  # Pin de disparo (TRIG)
ECHO = 24  # Pin de eco (ECHO)

GPIO.setmode(GPIO.BCM)
GPIO.setup(TRIG, GPIO.OUT)
GPIO.setup(ECHO, GPIO.IN)

try:
    while True:
        GPIO.output(TRIG, False)
        time.sleep(0.2)
        
        GPIO.output(TRIG, True)
        time.sleep(0.00001)
        GPIO.output(TRIG, False)
        
        while GPIO.input(ECHO) == 0:
            pulse_start = time.time()
            
        while GPIO.input(ECHO) == 1:
            pulse_end = time.time()
            
        pulse_duration = pulse_end - pulse_start
        distance = pulse_duration * 17150
        distance = round(distance, 2)
        
        print("Distancia:", distance, "cm")
        time.sleep(1)

except KeyboardInterrupt:
    GPIO.cleanup()
```

### Librerías

ara usar el sensor KY-038 con Python en Windows, debes centrarte en las librerías de Arduino para la placa Arduino y usar **pySerial** en Python para establecer la comunicación serial con la placa y leer los datos del sensor.

### Funcionalidades

El sensor KY-038 de proximidad ofrece una serie de funcionalidades que lo hacen adecuado para una variedad de aplicaciones. A continuación, se detallan algunas de las principales funcionalidades:

#### 1. Medición de Distancia en Tiempo Real

El sensor KY-038 utiliza tecnología ultrasónica para medir la distancia entre el sensor y un objeto cercano. Proporciona lecturas precisas y en tiempo real de la distancia en centímetros. Esta funcionalidad es esencial en aplicaciones donde se necesita conocer la distancia a un objeto.

#### 2. Detección de Obstáculos

El sensor KY-038 es ampliamente utilizado en sistemas de detección de obstáculos. Puede alertar cuando un objeto se encuentra a una distancia predefinida del sensor, lo que lo hace valioso en aplicaciones de seguridad, robots móviles y vehículos autónomos.

#### 3. Comunicación con Microcontroladores

El sensor KY-038 es fácil de integrar con microcontroladores como Arduino, Raspberry Pi y otros. Esto permite que los datos de distancia sean procesados y utilizados en una variedad de proyectos electrónicos y de automatización.

#### 4. Configuración Ajustable

El sensor KY-038 suele tener la capacidad de ajustar su sensibilidad y distancia de detección. Esto permite adaptar el sensor a diferentes situaciones y requisitos de proyecto, lo que lo hace versátil y personalizable.

## Enlaces

### Fuentes Consultadas

- [Enlace de la Fuente 1](https://blog.uelectronics.com/tarjetas-desarrollo/uso-de-los-sensores-de-sonido-ky-038-ky-037-para-controlar-el-encendido-de-un-foco/): Descripción de la especificación técnica del sensor KY-038.
- [Enlace de la Fuente 2](https://www.prometec.net/sensor-sonido-ky038/): Tutorial sobre cómo utilizar el sensor KY-038 en proyectos de electrónica.

### Páginas Oficiales

- [Enlace a la Página Oficial del Fabricante](https://forum.arduino.cc/t/ky-038-sound-sensor-1023/979147?page=2): Página web oficial del fabricante del sensor KY-038.
